using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleOrbit : MonoBehaviour
{
    private GameObject planet;
    private GameObject moon;
    private List<GameObject> moons = new List<GameObject>();
    private float radius;
    private float totalTime = 0;
    private PositionService positionService = new PositionService();

    public float rotationSpeed = 100;
    public int numNodes = 10;

    private void Start()
    {
        planet = Resources.Load<GameObject>("Prefabs/Planet");
        moon = Resources.Load<GameObject>("Prefabs/Moon");

        radius = planet.GetComponent<Renderer>().bounds.size.x;

        Instantiate(planet, new Vector3(0, 0, 0), Quaternion.identity);


        for (int i = 0; i < numNodes; i++)
        {
            moons.Add(Instantiate(moon, new Vector3(0, 0, 0), Quaternion.identity));
        }
    }

    private void Update()
    {
        totalTime += Time.deltaTime;
        Vector3 center = planet.transform.position;

        for (int i = 0; i < moons.Count; i++)
        {
            float spacing = ((360 / moons.Count) * i);
            Vector3 position = positionService.getPositionAroundCircle(center, spacing, radius, totalTime, rotationSpeed);
            // Quaternion rotation = Quaternion.identity; //Quaternion.FromToRotation(Vector3.forward, center - position);

            moons[i].transform.position = position;
            // moons[i].transform.rotation = rotation;
        }
    }
}
 