using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionService
{
    public Vector3 getPositionAroundCircle(Vector3 center, float spacing, float radius, float elapsedTime, float rotationSpeed)
    {
        //float angle = Random.value * 360;
        float angle = spacing + (elapsedTime * rotationSpeed);
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(angle * Mathf.Deg2Rad);
        pos.y = center.y + radius * Mathf.Cos(angle * Mathf.Deg2Rad);
        pos.z = center.z;
        return pos;
    }
}
