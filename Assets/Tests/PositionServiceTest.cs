using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class PositionServiceTest
{
    private PositionService positionService;
    private Vector3 center = new Vector3(0, 0, 0);

    [SetUp]
    public void Setup()
    {
        positionService = new PositionService();
    }

    [Test]
    public void lol()
    {
        Assert.AreEqual(1, 1);
    }

    [Test]
    public void lol2()
    {
        Vector3 actual = positionService.getPositionAroundCircle(center, 20, 50, 0, 10);
        Assert.AreEqual(actual.x, 17.1010056f);
        Assert.AreEqual(actual.y, 46.9846306f);
        Assert.AreEqual(actual.z, 0);
    }
}
